#include "calc.h"

char input[MAX_IO_ARRAY_SIZE];
uint16_t inputSize;

char output[MAX_IO_ARRAY_SIZE];
char output2[MAX_IO_ARRAY_SIZE]; // Sert de tampon
uint16_t outputSize;

void add_input(char c){
	input[inputSize] = c;
	inputSize++;
	if(inputSize >= MAX_IO_ARRAY_SIZE) inputSize = MAX_IO_ARRAY_SIZE - 1;
}

void del_input(){
	if(inputSize > 0) inputSize--;
}
void clear_input(){
	inputSize = 0;
}

void get_result_from_input() {
	int32_t result = get_result(input, inputSize);
	for(uint16_t i = 0; i < inputSize; i++) input[i] = 0;
	inputSize = 0;
	outputSize = 0;
	while(result != 0 && outputSize < MAX_IO_ARRAY_SIZE) {
		output2[outputSize] = (result %10) + '0';
		result /= 10;
		outputSize++;
	}
	uint16_t y = 0;
	if(outputSize == 0) {
		output[0] = '0';
		outputSize = 1;
	}
	else if (outputSize >= MAX_IO_ARRAY_SIZE && result > 0)
	{
		output[0] = "I";
		output[1] = "N";
		output[2] = "F";
		output[3] = "I";
		output[4] = "N";
		output[5] = "I";
		outputSize = 6;
	}
	else if (outputSize >= MAX_IO_ARRAY_SIZE && result < 0)
	{
		output[0] = "-";
		output[1] = "I";
		output[2] = "N";
		output[3] = "F";
		output[4] = "I";
		output[5] = "N";
		output[6] = "I";
		outputSize = 7;
	}
	else while(y < outputSize) {
		output[y] = output2[outputSize - 1 - y];
		y++;
	}

	return;
}

int32_t get_result(char* calcul, uint16_t size) {
	//PRIORITÉ OPERATOIRE :
	int16_t depth = 0;//getMaxDepth(calcul, size);
	// TODO : Insertion de parenthèses pour les priorités.

	// ITERATION DANS LA MEME PARENTHESE :
	int32_t i = 0;
	int32_t result = 0;
	int32_t leftValue = 0;
	bool leftValueConfirmed = false;
	while(i < size)
	{
		int32_t rightValue = 0;
		bool rightValueConfirmed = false;
		// TODO : gerer les parentheses.
		if(depth == 0)
		{
			//CALCUL INTERNE :
			char* subcalcul = &calcul[i];
			uint16_t baseI = i;
			uint16_t subsize = size - i;
			int32_t y = 0;
			bool negateLeftValue = false, negateRightValue = false;
			char operator = 'z';
			while((i < size) && !rightValueConfirmed)
			{
				if(subcalcul[i] != ')')
				{
					if(isANumber(subcalcul[i - baseI]))
					{
						int8_t sizeOfValue = 0;
						if(!leftValueConfirmed)
						{
							leftValue = mapStringToInt32(subcalcul + (i - baseI), subsize, &sizeOfValue);
							leftValueConfirmed = true;
						}
						else
						{
							rightValue = mapStringToInt32(subcalcul + (i - baseI), subsize, &sizeOfValue);
							rightValueConfirmed = true;
						}
						i += sizeOfValue;
					}
					else
					{
						if(subcalcul[i] == '-')
						{
							if(!leftValueConfirmed)
								negateLeftValue = !negateLeftValue;
							else
								negateRightValue = !negateRightValue;
						}
						else
							operator = subcalcul[i - baseI];
						i++;
					}
				}
				else
					break;
			}

			if(leftValueConfirmed && rightValueConfirmed)
			{
				int32_t inter;
				if(negateLeftValue) leftValue = -leftValue;
				if(negateRightValue) rightValue = -rightValue;
				if(operator == 'z')
					operator = '+';
				switch(operator){
				case '+':
					result = leftValue + rightValue;
					break;
				case '-':
					result = leftValue - rightValue;
					break;
				case '*':
					result = leftValue * rightValue;
					break;
				case '/':
					result = leftValue / rightValue;
					break;
				case '^':
					inter = leftValue;
					if(rightValue > 0)
					{
						for(int i = 0; i < rightValue - 1; i++)
							inter *= leftValue;
						result = inter;
					}
					else if (rightValue == 0){
						result = 1;
					}
					else {
						for(int i = 0; i > rightValue; i--)
							inter /= leftValue;
						result = inter;
					}

					break;
				default:
					return -1;
					break;
				}
			}
			else result = leftValue;
		}
		leftValueConfirmed = true;
		leftValue = result;
	}
	return result;
}

bool isAnOperator(char c)
{
	if((c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '^') || (c == '(') || (c == ')'))
		return true;
	return false;
}

bool isANumber(char c)
{
	if((c >= '0') && (c <= '9'))
		return true;
	return false;
}

int16_t getFirstOperator(char chara, char* string, uint16_t size) {
	for(int16_t i = 0; i < size; i++)
		if(string[i] == chara)
			return i;
	return -1;
}

int32_t mapStringToInt32(char* string, uint16_t size, uint8_t* sizeOfValue) {
	int32_t result = 0;
	for(int16_t i = 0; (i < size); i++)
	{
		if (isANumber(string[i])) {
			*sizeOfValue = *sizeOfValue + 1;
			result = (string[i] - '0') + (result * 10);
		}
		else
			break;
	}
	return result;
}

int16_t getAmountOfNumeralsInValueFromString(uint16_t posOfFirstValue, char* string, uint16_t sizeOfString) {
	int16_t i = 0;
	for(; i < sizeOfString; i++)
		if(isANumber(string[i]))
			break;
	return i;
}

int16_t getFirstChara(char chara, char* string, uint16_t size) {
	for(int16_t i; i < size; i++)
		if(string[i] == chara)
			return i;
	return -1;
}

int16_t getNthChara(char chara, int16_t n, char* string, uint16_t size) {
	int16_t y;
	for(int16_t i; i < size; i++)
		if(string[i] == chara)
		{
			y++;
			if(y == n)
				return i;
		}
	return -1;
}

int16_t getMaxDepth(char* string, uint16_t size) {
	int16_t y = 0, z = 0;
	for(int16_t i; i < size; i++)
	{
		if(string[i]== '(')
			y++;
		if(string[i]== ')')
			y--;
		if(y >  z) z = y;
	}
	return z;
}

int16_t countChara(char chara, char* string, uint16_t size) {
	int16_t y = 0;
	for(int16_t i; i < size; i++)
		if(string[i] == chara) y++;
	return y;
}
