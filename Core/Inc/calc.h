#include "stdbool.h"
#include "stdint.h"

#define MAX_IO_ARRAY_SIZE 100

int32_t get_result(char*, uint16_t);
int16_t getMaxDepth(char*, uint16_t);
int32_t mapStringToInt32(char*, uint16_t, uint8_t*);
bool isANumber(char);
bool isAnOperator(char);

void add_input(char);
void del_input();
void clear_input();

void get_result_from_input();
